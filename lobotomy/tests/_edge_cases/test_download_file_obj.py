import lobotomy
import io


@lobotomy.Patch()
def test_s3_download_fileobj(lobotomized: lobotomy.Lobotomy):
    """Should handle s3.download_fileobj correctly despite it being an augmentation."""
    lobotomized.add_call("s3", "download_fileobj", {"Fileobj": "Hello World!"})
    session = lobotomized()
    client = session.client("s3")

    data = io.BytesIO()
    client.download_fileobj(Bucket="bar", Key="baz", Fileobj=data)

    call = lobotomized.get_service_calls("s3", "download_fileobj")[0]
    assert call.request["Fileobj"] == data
    assert call.request["Bucket"] == "bar"
    assert call.request["Key"] == "baz"
    assert data.getvalue().decode() == "Hello World!"
