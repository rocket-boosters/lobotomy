import typing

import boto3
import lobotomy

import botocore.exceptions


@lobotomy.patch()
def test_general_client_error(lobotomized: lobotomy.Lobotomy):
    """Should handle client.exceptions.ClientError for general error handling."""
    lobotomized.add_error_call(
        "s3",
        "delete_object",
        error_code="AccessDenied",
        error_message="Doesn't look like this exists.",
    )
    client = boto3.Session().client("s3")

    error: typing.Optional[Exception] = None
    try:
        client.delete_object(
            Bucket="does-not-exist",
            Key="does-not-exist",
        )
    except client.exceptions.ClientError as e:
        error = e

    assert error is not None
    assert typing.cast(lobotomy.ClientError, error)


@lobotomy.patch()
def test_botocore_error(lobotomized: lobotomy.Lobotomy):
    """Should handle botocore ClientError for general error handling."""
    lobotomized.add_error_call(
        "s3",
        "delete_object",
        error_code="AccessDenied",
        error_message="Doesn't look like this exists.",
    )
    client = boto3.Session().client("s3")

    error: typing.Optional[Exception] = None
    try:
        client.delete_object(
            Bucket="does-not-exist",
            Key="does-not-exist",
        )
    except botocore.exceptions.ClientError as e:
        error = e

    assert error is not None
    assert typing.cast(lobotomy.ClientError, error)
